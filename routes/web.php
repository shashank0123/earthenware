<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\AdminDashboardController;
use App\Http\Controllers\UserActionController;
use App\Http\Controllers\WebsiteSettingController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix'=> 'admin', 'middleware'=> ['admin:admin']], function(){
	Route::get('/login', [AdminController::class, 'getLogin']);
	Route::post('/login', [AdminController::class, 'store'])->name('admin.login');
});

Route::middleware(['auth:sanctum','admin', 'verified'])->get('/admin/dashboard', function () {
    return view('admin.dashboard');
})->name('admin.dashboard');



Route::middleware(['auth:sanctum','web', 'verified'])->get('/dashboard', function () {
    return view('dashboard.dashboard');
})->name('dashboard');


Route::group(['prefix'=>'admin', 'middleware' => ['auth:sanctum','admin', 'verified', ]], function(){
	// Route::get('/dashboard', [AdminDashboardController::class, 'index']);
	Route::get('/settings', [WebsiteSettingController::class, 'getSettingPage']);
});

Route::group(['prefix'=>'members', 'middleware' => ['auth:sanctum', 'verified', ]], function(){
	// Route::get('/dashboard', [AdminDashboardController::class, 'index']);
	Route::get('/profile', [UserActionController::class, 'getProfile'])->name('user.profile');
	Route::get('/profile/edit', [UserActionController::class, 'editProfileForm'])->name('user.profile.edit');
	Route::post('/profile/edit', [UserActionController::class, 'updateProfile'])->name('user.profile.update');
	Route::get('/password', [UserActionController::class, 'editPasswordForm'])->name('user.password.view');
	Route::post('/password', [UserActionController::class, 'updatePassword'])->name('user.password.update');
});
Route::get('/user/logout', [UserActionController::class, 'logout'])->name('user.logout');

Route::get('/admin/logout', [AdminController::class, 'destroy'])->name('admin.logout');
