<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@4living">
    <meta name="twitter:creator" content="@4living">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Starlight">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://4living.me/image/logo.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://4living.com">
    <meta property="og:title" content="Starlight">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://4living.com/image/logo.png">
    <meta property="og:image:secure_url" content="http://4living.me/image/logo.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="4living">

    <title>Backstage Admin panel - 4living</title>

    <!-- vendor css -->
    <link href="{{ asset('/admin/lib/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{ asset('/admin/lib/Ionicons/css/ionicons.css')}}" rel="stylesheet">
    <link href="{{ asset('/admin/lib/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet">
    <link href="{{ asset('/admin/lib/rickshaw/rickshaw.min.css')}}" rel="stylesheet">

    <!-- Starlight CSS -->
    <link rel="stylesheet" href="{{ asset('/admin/css/starlight.css')}}">
  </head>

  <body>

    @include('admin.layouts.sidebaradmin')

    @include('admin.layouts.headbaradmin')

    @include('admin.layouts.rightbaradmin')
    
    <div class="sl-mainpanel">
      @yield('breadcrumb')

      <div class="sl-pagebody">
    @yield('mainarea')
    </div><!-- sl-pagebody -->
    <footer class="sl-footer">
        <div class="footer-left">
          <div class="mg-b-2">Copyright &copy; 2017. 4Living. All Rights Reserved.</div>
          <div>Made by 4living.</div>
        </div>
        <div class="footer-right d-flex align-items-center">
          <span class="tx-uppercase mg-r-10">Share:</span>
          <a target="_blank" class="pd-x-5" href="https://www.facebook.com/sharer/sharer.php?u=http%3A//4living.com"><i class="fa fa-facebook tx-20"></i></a>
          <a target="_blank" class="pd-x-5" href="https://twitter.com/home?status=4living%20now%20liveat%20http%3A//4living.in"><i class="fa fa-twitter tx-20"></i></a>
        </div>
      </footer>
    </div><!-- sl-mainpanel -->
    <!-- ########## END: MAIN PANEL ########## -->

    <script src="{{ asset('/admin/lib/jquery/jquery.js') }}"></script>
    <script src="{{ asset('/admin/lib/popper.js/popper.js') }}"></script>
    <script src="{{ asset('/admin/lib/bootstrap/bootstrap.js') }}"></script>
    <script src="{{ asset('/admin/lib/jquery-ui/jquery-ui.js') }}"></script>
    <script src="{{ asset('/admin/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js') }}"></script>
    <script src="{{ asset('/admin/lib/jquery.sparkline.bower/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('/admin/lib/d3/d3.js') }}"></script>
    <script src="{{ asset('/admin/lib/rickshaw/rickshaw.min.js') }}"></script>
    <script src="{{ asset('/admin/lib/chart.js/Chart.js') }}"></script>
    <script src="{{ asset('/admin/lib/Flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('/admin/lib/Flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('/admin/lib/Flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('/admin/lib/flot-spline/jquery.flot.spline.js') }}"></script>

    <script src="{{ asset('/admin/js/starlight.js') }}"></script>
    <script src="{{ asset('/admin/js/ResizeSensor.js') }}"></script>
    <script src="{{ asset('/admin/js/dashboard.js') }}"></script>
  </body>
</html>
